#include <stdio.h>
#define MAXVER 50

int img[100][100];

typedef struct Bucket
{
    int yMax;
    int yMin;
    int x;
    int sign;
    float dX;
    float dY;
    // float slopeinverse;
    double sum;
} Bucket;

typedef struct EdgeTable
{
    // the array will give the scanline number
    // The edge table (ET) with edges entries sorted
    // in increasing y and x of the lower end
    int countEdgeBucket; //no. of edgebuckets
    Bucket buckets[MAXVER];
} EdgeTable;

EdgeTable createEdges(int n, int x[], int y[]);
void drawPolygon(int n, int x[], int y[]);
void sort(EdgeTable &finalEdgeTable);
void sortTheActiveList(EdgeTable &activeList);
void remove(EdgeTable &edgeTable, int x);
void processEdgeTable(EdgeTable &edgeTable);

void init_mat(int m[100][100], int x);
void createPGM();
int abs(int x);

int main()
{
    int n;
    int x[6],y[6];
    // n=5;
    // int x[5],y[5];
    // x[0]=10;
    // y[0]=0;
    // x[1]=15;
    // y[1]=5;
    // x[2]=15;
    // y[2]=40;
    // x[3]=0;
    // y[3]=40;
    // x[4]=0;
    // y[4]=5;

    // n=5;
    // int x[5],y[5];
    // x[0]=3;
    // y[0]=0;
    // x[1]=5;
    // y[1]=2;
    // x[2]=5;
    // y[2]=5;
    // x[3]=0;
    // y[3]=5;
    // x[4]=0;
    // y[4]=2;

    // n=3;
    // int x[3],y[3];
    // x[0]=10;
    // y[0]=0;
    // x[1]=50;
    // y[1]=5;
    // x[2]=5;
    // y[2]=5;

    // n=4;
    // int x[4],y[4];
    // x[0]=0;
    // y[0]=0;
    // x[1]=3;
    // y[1]=0;
    // x[2]=3;
    // y[2]=3;
    // x[3]=0;
    // y[3]=3;

    // n=6;
    // int x[6],y[6];
    // x[0]=10;
    // y[0]=0;
    // x[1]=30;
    // y[1]=0;
    // x[2]=40;
    // y[2]=15;
    // x[3]=30;
    // y[3]=30;
    // x[4]=10;
    // y[4]=30;
    // x[5]=0;
    // y[5]=15;

    n=6;
    x[0]=40;
    y[0]=10;
    x[1]=60;
    y[1]=10;
    x[2]=50;
    y[2]=30;
    x[3]=60;
    y[3]=50;
    x[4]=40;
    y[4]=50;
    x[5]=30;
    y[5]=30;

    // n=6;
    // int x[6],y[6];
    // x[0]=0;
    // y[0]=10;
    // x[1]=5;
    // y[1]=0;
    // x[2]=10;
    // y[2]=15;
    // x[3]=15;
    // y[3]=12;
    // x[4]=16;
    // y[4]=20;
    // x[5]=0;
    // y[5]=20;

    // int x[MAXVER], y[MAXVER];
    // printf("Enter number of vertices: \n");
    // scanf("%d", &n);

    // for (int i = 0; i < n; i++)
    // {
    //     printf("\nEnter coordinates of vertex %d [ x%d y%d ]: ", i, i, i);
    //     scanf("%d %d", &x[i], &y[i]);
    // }

    // for (int i = 0; i < n; i++)
    // {
    //     x[i] = x[i] + 30;
    //     y[i] = y[i] + 30;
    // }

    drawPolygon(n, x, y);
    return 0;
}

//
// Draw a filled polygon in the Canvas C.
//

// The polygon has n distinct vertices.  The coordinates of the vertices
// making up the polygon are stored in the x and y arrays.  The ith
// vertex will have coordinate (x[i],y[i]).
//
// @param n - number of vertices
// @param x - x coordinates
// @param y - y coordinates
///

void drawPolygon(int n, int x[], int y[])
{
    // Create edge table
    init_mat(img, 0);
    EdgeTable finalEdgeTable = createEdges(n, x, y);
    // Sort edges by minY
    sort(finalEdgeTable);
    processEdgeTable(finalEdgeTable);
    createPGM();
}

EdgeTable createEdges(int n, int x[], int y[])
{
    EdgeTable ET;
    int k, knext;
    ET.countEdgeBucket = 0;
    for (int i = 0; i < n; i++)
    {
        k = i % (n);
        knext = (i + 1) % n;
        if (y[k] != y[knext]) //if(x[k]!=x[knext])////
        {
            Bucket edge;
            edge.yMin = (y[k] < y[knext]) ? y[k] : y[knext];
            edge.yMax = (y[k] > y[knext]) ? y[k] : y[knext];
            edge.dX = x[knext] - x[k];
            edge.dY = y[knext] - y[k];
            edge.sum = 0;
            if (edge.dX != 0)
            {
                edge.sign = ((edge.dY / edge.dX) < 0) ? -1 : 1;
            }
            else
            {
                edge.sign = 0;
            }

            // if(edge.dY == 0) edge.slopeinverse =1.0;
            // if(edge.dX == 0) edge.slopeinverse =0.0;

            // if((edge.dY!=0)&&(edge.dX!=0)) /*- calculate inverse slope -*/
            // {
            // edge.slopeinverse = (float) edge.dX/edge.dY;
            // }

            edge.dX = abs(edge.dX);
            edge.dY = abs(edge.dY);
            edge.x = (y[k] < y[knext]) ? x[k] : x[knext];
            ET.buckets[ET.countEdgeBucket] = edge;
            ET.countEdgeBucket++;
            //add bucket to edge table
        }
    }
    return ET;
}

void sort(EdgeTable &finalEdgeTable)
{
    Bucket temp;
    int n = finalEdgeTable.countEdgeBucket;
    for (int i = 0; i < n - 1; i++)
    {
        for (int j = 0; j < n - i - 1; j++)
        {
            if (finalEdgeTable.buckets[j].yMin > finalEdgeTable.buckets[j + 1].yMin)
            {
                temp = finalEdgeTable.buckets[j];
                finalEdgeTable.buckets[j] = finalEdgeTable.buckets[j + 1];
                finalEdgeTable.buckets[j + 1] = temp;
            }
        }
    }
}

// Given the edge table of the polygon, fill the polygons

// @param edgeTable The polygons edge table representation
void processEdgeTable(EdgeTable &edgeTable)
{
    int scanline = edgeTable.buckets[0].yMin;
    EdgeTable al;
    al.countEdgeBucket = 0;
    while (edgeTable.countEdgeBucket != 0) //edge table is NOT empty
    {
        // Remove edges from the active list if y == ymax
        if (al.countEdgeBucket != 0)
        { //active list is NOT empty
            for (int i = al.countEdgeBucket - 1; i >= 0; i--)
            {
                if (al.buckets[i].yMax == scanline) //if (current bucket's ymax == current scanline)
                {
                    remove(al, i);        //bucket from active list
                    remove(edgeTable, i); //bucket from edge table
                }
            }
        }
        // Add edge from edge table to active list if y == ymin
        for (int i = 0; i < edgeTable.countEdgeBucket; i++) //iterate through the bucket in the edge table) {
        {
            if (edgeTable.buckets[i].yMin == scanline) //if (bucket's ymin == scanline)
            {
                al.buckets[al.countEdgeBucket] = edgeTable.buckets[i];
                al.countEdgeBucket++; //add bucket to active list
            }
        }
        // Sort active list by x position and slope
        sortTheActiveList(al);

        // Fill the polygon pixel
        for (int i = 0; i < al.countEdgeBucket; i += 2)
        { //iterate through the active list) {
            for (int x = al.buckets[i].x; x <= al.buckets[i + 1].x; x++)
            {                         //from vertex1.x to vertex2.x of the bucket) {
                img[x][scanline] = 1; //setPixelColor()
            }
        }
        // Increment X variables of buckets based on the slope
        for (int i = 0; i < al.countEdgeBucket; i++) //all buckets in the active list)
        {
            if (al.buckets[i].dX != 0)
            {
                al.buckets[i].sum += al.buckets[i].dX;        //bucket's sum += bucket's dX
                while (al.buckets[i].sum >= al.buckets[i].dY) //bucket's sum >= bucket's dY)
                {
                    if (al.buckets[i].sign >= 0) //increment or decrement bucket's X depending on sign of   bucket's slope
                    {
                        al.buckets[i].x++;
                    }
                    else
                    {
                        al.buckets[i].x--;
                    }
                    //edge's sum -= dY
                    al.buckets[i].sum -= al.buckets[i].dY;
                }
            }
        }
        //updatexbyslopeinv(al);
        scanline++;
    }
}

void sortTheActiveList(EdgeTable &activeList)
{
    Bucket temp;
    int n = activeList.countEdgeBucket;
    for (int i = 0; i < n - 1; i++)
    {
        for (int j = 0; j < n - i - 1; j++)
        {
            if (activeList.buckets[j].x > activeList.buckets[j + 1].x)
            {
                temp = activeList.buckets[j];
                activeList.buckets[j] = activeList.buckets[j + 1];
                activeList.buckets[j + 1] = temp;
            }
        }
    }
}

void remove(EdgeTable &edgeTable, int x)
{
    int n = edgeTable.countEdgeBucket;
    for (int i = x; i < n; i++)
    {
        edgeTable.buckets[i] = edgeTable.buckets[i + 1];
    }
    edgeTable.countEdgeBucket--;
}

// void updatexbyslopeinv(EdgeTable& Tup)
// {
//     int i;

//     for (i=0; i<Tup.countEdgeBucket; i++)
//     {
//         Tup.buckets[i].x = (int)((Tup.buckets[i].x + Tup.buckets[i].slopeinverse)+0.5) ;
//     }
// }

void init_mat(int m[100][100], int x)
{
    for (int i = 0; i < 100; i++)
    {
        for (int j = 0; j < 100; j++)
            m[i][j] = x;
    }
}

int abs(int x)
{
    if (x >= 0)
        return x;
    else
        return -1 * x;
}

void createPGM()
{
    FILE *pgmimg;
    int i, j;
    pgmimg = fopen("assignment-04.pgm", "wb");
    fprintf(pgmimg, "P2\n");
    fprintf(pgmimg, "100 100\n");
    fprintf(pgmimg, "1\n");
    
    for (i = 0; i < 100; i++)
    {
        for (j = 0; j < 100; j++)
        {
            int temp = img[j][i];
            fprintf(pgmimg, "%d ", temp);
        }
    }
    fclose(pgmimg);
}