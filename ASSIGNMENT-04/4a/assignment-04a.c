#include <stdio.h>
#define MAXVER 50

int img[100][100];

typedef struct Bucket
{
    int yMax;
    int yMin;
    int x;
    int sign;
    float dX;
    float dY;
    double sum;
} Bucket;

typedef struct EdgeTable
{
    int countEdgeBucket;
    Bucket buckets[MAXVER];
} EdgeTable;

EdgeTable createEdges(int n, int x[], int y[]);
void drawPolygon(int n, int x[], int y[]);
void sort(EdgeTable &finalEdgeTable);
void sortTheActiveList(EdgeTable &activeList);
void remove(EdgeTable &edgeTable, int x);
void processEdgeTable(EdgeTable &edgeTable);

void init_mat(int m[100][100], int x);
void createPGM();
int abs(int x);

int main()
{
    int n;
    int x[MAXVER], y[MAXVER];

    // printf("Enter number of vertices: \n");
    // scanf("%d", &n);
    // for (int i = 0; i < n; i++)
    // {
    //     printf("\nEnter coordinates of vertex %d [ x%d y%d ]: ", i, i, i);
    //     scanf("%d %d", &x[i], &y[i]);
    // }

    n=6;
    x[0]=40;
    y[0]=10;
    x[1]=60;
    y[1]=10;
    x[2]=70;
    y[2]=30;
    x[3]=60;
    y[3]=50;
    x[4]=40;
    y[4]=50;
    x[5]=30;
    y[5]=30;

    drawPolygon(n, x, y);
    return 0;
}

void drawPolygon(int n, int x[], int y[])
{
    init_mat(img, 0);
    EdgeTable finalEdgeTable = createEdges(n, x, y);
    sort(finalEdgeTable);
    processEdgeTable(finalEdgeTable);
    createPGM();
}

EdgeTable createEdges(int n, int x[], int y[])
{
    EdgeTable ET;
    int k, knext;
    ET.countEdgeBucket = 0;
    for (int i = 0; i < n; i++)
    {
        k = i % (n);
        knext = (i + 1) % n;
        if (y[k] != y[knext])
        {
            Bucket edge;
            edge.yMin = (y[k] < y[knext]) ? y[k] : y[knext];
            edge.yMax = (y[k] > y[knext]) ? y[k] : y[knext];
            edge.dX = x[knext] - x[k];
            edge.dY = y[knext] - y[k];
            edge.sum = 0;
            if (edge.dX != 0)
            {
                edge.sign = ((edge.dY / edge.dX) < 0) ? -1 : 1;
            }
            else
            {
                edge.sign = 0;
            }

            edge.dX = abs(edge.dX);
            edge.dY = abs(edge.dY);
            edge.x = (y[k] < y[knext]) ? x[k] : x[knext];
            ET.buckets[ET.countEdgeBucket] = edge;
            ET.countEdgeBucket++;
        }
    }
    return ET;
}

void sort(EdgeTable &finalEdgeTable)
{
    Bucket temp;
    int n = finalEdgeTable.countEdgeBucket;
    for (int i = 0; i < n - 1; i++)
    {
        for (int j = 0; j < n - i - 1; j++)
        {
            if (finalEdgeTable.buckets[j].yMin > finalEdgeTable.buckets[j + 1].yMin)
            {
                temp = finalEdgeTable.buckets[j];
                finalEdgeTable.buckets[j] = finalEdgeTable.buckets[j + 1];
                finalEdgeTable.buckets[j + 1] = temp;
            }
        }
    }
}

void processEdgeTable(EdgeTable &edgeTable)
{
    int scanline = edgeTable.buckets[0].yMin;
    EdgeTable al;
    al.countEdgeBucket = 0;
    while (edgeTable.countEdgeBucket != 0)
    {
        if (al.countEdgeBucket != 0)
        {
            for (int i = al.countEdgeBucket - 1; i >= 0; i--)
            {
                if (al.buckets[i].yMax == scanline)
                {
                    remove(al, i);
                    remove(edgeTable, i);
                }
            }
        }

        for (int i = 0; i < edgeTable.countEdgeBucket; i++)
        {
            if (edgeTable.buckets[i].yMin == scanline)
            {
                al.buckets[al.countEdgeBucket] = edgeTable.buckets[i];
                al.countEdgeBucket++;
            }
        }
        
        sortTheActiveList(al);

        for (int i = 0; i < al.countEdgeBucket; i += 2)
        {
            for (int x = al.buckets[i].x; x <= al.buckets[i + 1].x; x++)
            {             
                img[x][scanline] = 1;
            }
        }

        for (int i = 0; i < al.countEdgeBucket; i++)
        {
            if (al.buckets[i].dX != 0)
            {
                al.buckets[i].sum += al.buckets[i].dX;
                while (al.buckets[i].sum >= al.buckets[i].dY)
                {
                    if (al.buckets[i].sign >= 0)
                    {
                        al.buckets[i].x++;
                    }
                    else
                    {
                        al.buckets[i].x--;
                    }
                    al.buckets[i].sum -= al.buckets[i].dY;
                }
            }
        }
        scanline++;
    }
}

void sortTheActiveList(EdgeTable &activeList)
{
    Bucket temp;
    int n = activeList.countEdgeBucket;
    for (int i = 0; i < n - 1; i++)
    {
        for (int j = 0; j < n - i - 1; j++)
        {
            if (activeList.buckets[j].x > activeList.buckets[j + 1].x)
            {
                temp = activeList.buckets[j];
                activeList.buckets[j] = activeList.buckets[j + 1];
                activeList.buckets[j + 1] = temp;
            }
        }
    }
}

void remove(EdgeTable &edgeTable, int x)
{
    int n = edgeTable.countEdgeBucket;
    for (int i = x; i < n; i++)
    {
        edgeTable.buckets[i] = edgeTable.buckets[i + 1];
    }
    edgeTable.countEdgeBucket--;
}


void init_mat(int m[100][100], int x)
{
    for (int i = 0; i < 100; i++)
    {
        for (int j = 0; j < 100; j++)
            m[i][j] = x;
    }
}

int abs(int x)
{
    if (x >= 0)
        return x;
    else
        return -1 * x;
}

void createPGM()
{
    FILE *pgmimg;
    int i, j;
    pgmimg = fopen("assignment-04a.pgm", "wb");
    fprintf(pgmimg, "P2\n");
    fprintf(pgmimg, "100 100\n");
    fprintf(pgmimg, "1\n");
    
    for (i = 0; i < 100; i++)
    {
        for (j = 0; j < 100; j++)
        {
            int temp = img[j][i];
            fprintf(pgmimg, "%d ", temp);
        }
    }
    fclose(pgmimg);
}