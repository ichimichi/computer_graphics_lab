#include <stdio.h>
#include <conio.h>
#include <math.h>
#include <stdlib.h>
#define PI 3.14159265

int img[100][100];

struct xy
{
    int x;
    int y;
};

struct ellipse
{
    struct xy center;
    int Rx;
    int Ry;
};

struct parallelogram
{
    int a;
    int b;
    struct xy center;
};

void drawShape(struct parallelogram r, struct ellipse e, double theta);
int abs(int x);
void BRHAM(int X0, int Y0, int X1, int Y1, int img[100][100]);

void draw_prllgram(struct parallelogram r, int shear, int img[100][100]);
void shear(double shx);

void midPointEllipse(struct ellipse e, int img[100][100]);
void plotPointsInQuadrants(int, int, int, int, int img[100][100]);
void rotateWithPivot(double theta, int xf, int yf, int img[100][100]);

void MatMul(float mat1[3][3], int mat2[3][1], int res[3][1]);

void init_identity(float x[3][3]);
void init_mat(int m[100][100], int x);
void overrideImage(int img1[100][100], int img2[100][100]);
void Translate(int xf, int yf, int img[100][100]);
void createFile();

void mergeImage(int img1[100][100], int img2[100][100]);


void reflectWithMirror(int img[100][100], int axis);


int main()
{
    struct ellipse e;
    struct parallelogram r;
    int i, j;
    double theta;

    printf("\nEnter a     : ");
    scanf("%d", &(r.a));

    printf("\nEnter b : ");
    scanf("%d", &(r.b));

    printf("\nEnter the center (x,y) : ");
    scanf("%d,%d", &r.center.x, &r.center.y);
    e.center.x = r.center.x;
    e.center.y = r.center.y;
    // printf("\nEnter the center (x,y) : ");
    // scanf("%d,%d", &e.center.x, &e.center.y);
    // printf("\nEnter the semi-major axis (Rx)   : ");
    // scanf("%d", &e.Rx);
    e.Rx = r.b / 2;
    printf("\nEnter the semi-minor axis (Ry)   : ");
    scanf("%d", &e.Ry);
    // printf("\nEnter theta   : ");
    // scanf("%lf", &theta);

    drawShape(r, e, -90.0);

    createFile();

    return 0;
}

void drawShape(struct parallelogram r, struct ellipse e, double theta)
{
    init_mat(img, 1);
    int img1[100][100];
    init_mat(img1, 1);
    draw_prllgram(r, 5, img1);
    int img2[100][100];
    init_mat(img2, 1);
    midPointEllipse(e, img2);
    rotateWithPivot(theta, e.center.x, e.center.y, img2);
    mergeImage(img1, img2);
    reflectWithMirror(img1, 100);
    overrideImage(img, img1);
    BRHAM(0, 100 / 2, 100, 100 / 2, img);

}

void rotateWithPivot(double theta, int xf, int yf, int img[100][100])
{
    float R[3][3];
    int P[3][1];
    int P1[3][1];
    int i, j;
    int img_temp[100][100];

    init_mat(img_temp, 1);
    xf = xf - 1;
    theta = (theta * PI) / 180;

    R[0][0] = cos(theta);
    R[0][1] = -1 * sin(theta);
    R[0][2] = xf * (1 - cos(theta)) + yf * sin(theta);
    R[1][0] = sin(theta);
    R[1][1] = cos(theta);
    R[1][2] = yf * (1 - cos(theta)) - xf * sin(theta);
    R[2][0] = 0;
    R[2][1] = 0;
    R[2][2] = 1;

    for (i = 0; i < 100; i++)
    {
        for (j = 0; j < 100; j++)
        {
            if (img[i][j] == 0)
            {
                P[0][0] = i;
                P[1][0] = j;
                P[2][0] = 1;

                MatMul(R, P, P1);
                img_temp[P1[0][0]][P1[1][0]] = 0;
            }
        }
    }

    overrideImage(img, img_temp);
}

void shear(double shx)
{
    float S[3][3];
    int P[3][1];
    int P1[3][1];
    int i, j;
    int img_temp[100][100];

    init_mat(img_temp, 1);
    init_identity(S);

    S[0][1] = shx;
    S[0][2] = 50 - 5;

    for (i = 0; i < 100; i++)
    {
        for (j = 0; j < 100; j++)
        {
            if (img[i][j] == 0)
            {
                P[0][0] = i;
                P[1][0] = j;
                P[2][0] = 1;

                MatMul(S, P, P1);
                img_temp[P1[0][0]][P1[1][0]] = 0;
            }
        }
    }
}

void reflectWithMirror(int img[100][100], int axis)
{
    float Re[3][3];
    int P[3][1];
    int P1[3][1];
    int i, j;
    int img_temp[100][100];

    init_mat(img_temp, 1);
    init_identity(Re);

    Re[1][1] = -1;

    for (i = 0; i < 100; i++)
    {
        for (j = 0; j < 100; j++)
        {
            if (img[i][j] == 0)
            {
                P[0][0] = i;
                P[1][0] = j;
                P[2][0] = 1;

                MatMul(Re, P, P1);
                img_temp[P1[0][0]][P1[1][0]] = 0;
            }
        }
    }
    Translate(0, axis, img_temp);

    mergeImage(img, img_temp);

    //overrideImage(img,img_temp);
}

void Translate(int xf, int yf, int img[100][100])
{
    float T[3][3];
    int P[3][1];
    int P1[3][1];
    int i, j;
    int img_temp[100][100];
    init_mat(img_temp, 1);

    init_identity(T);
    T[0][2] = xf;
    T[1][2] = yf;

    for (i = 0; i < 100; i++)
    {
        for (j = 0; j < 100; j++)
        {
            if (img[i][j] == 0)
            {
                P[0][0] = i;
                P[1][0] = j;
                P[2][0] = 1;
                MatMul(T, P, P1);
                img_temp[P1[0][0]][P1[1][0]] = 0;
            }
        }
    }

    overrideImage(img, img_temp);
}

void overrideImage(int img1[100][100], int img2[100][100])
{
    int i, j;

    for (i = 0; i < 100; i++)
    {
        for (j = 0; j < 100; j++)
        {
            img1[i][j] = img2[i][j];
        }
    }
}

void mergeImage(int img1[100][100], int img2[100][100])
{
    int i, j;

    for (i = 0; i < 100; i++)
    {
        for (j = 0; j < 100; j++)
        {
            if (img1[i][j] != 0)
                img1[i][j] = img2[i][j];
        }
    }
}

void MatMul(float mat1[3][3], int mat2[3][1], int res[3][1])
{
    int i, j, k;
    for (i = 0; i < 3; i++)
    {
        for (j = 0; j < 1; j++)
        {
            res[i][j] = 0;
            for (k = 0; k < 3; k++)
            {
                res[i][j] += (int)((mat1[i][k] * mat2[k][j]) + 0.5);
            }
        }
    }
}

void midPointEllipse(struct ellipse e, int img[100][100])
{
    int pk;
    int Rx_sq = e.Rx * e.Rx;
    int Ry_sq = e.Ry * e.Ry;

    int two_Rx_sq = 2 * Rx_sq;
    int two_Ry_sq = 2 * Ry_sq;

    int x = 0;
    int y = e.Ry;

    int px = 0;
    int py = two_Rx_sq * e.Ry;

    plotPointsInQuadrants(e.center.x, e.center.y, x, y, img);

    pk = ((int)((Ry_sq + 0.25 * Rx_sq - Rx_sq * e.Ry) + 0.5));

    while (px < py)
    {

        x += 1;
        px += two_Ry_sq;

        if (pk < 0)
        {
            pk += px + Ry_sq;
        }
        else
        {
            y -= 1;
            py -= two_Rx_sq;
            pk += px + Ry_sq - py;
        }

        plotPointsInQuadrants(e.center.x, e.center.y, x, y, img);
    }

    pk = ((int)((Ry_sq * (x + 0.5) * (x + 0.5) + Rx_sq * (y - 1) * (y - 1) - Rx_sq * Ry_sq) + 0.5));

    while (y > 0)
    {
        y--;
        py -= two_Rx_sq;
        if (pk > 0)
        {
            pk += Rx_sq - py;
        }
        else
        {
            x++;
            px += two_Ry_sq;
            pk += Rx_sq - py + px;
        }

        plotPointsInQuadrants(e.center.x, e.center.y, x, y, img);
    }
}

void plotPointsInQuadrants(int xc, int yc, int x, int y, int img[100][100])
{

    img[xc + x][yc + y] = 0;
    img[xc + x][yc - y] = 0;
    img[xc - x][yc + y] = 0;
    img[xc - x][yc - y] = 0;
}

int abs(int x)
{

    if (x >= 0)
    {
        return x;
    }
    else
    {
        return -1 * x;
    }
}

void init_mat(int m[100][100], int x)
{
    int i, j;
    for (i = 0; i < 100; i++)
    {
        for (j = 0; j < 100; j++)
        {
            m[i][j] = x;
        }
    }
}

void createFile()
{
    FILE *fimage;
    int i, j;
    fimage = fopen("assignment-03.pgm", "wb");
    fprintf(fimage, "P2\n");
    fprintf(fimage, "100 100\n");
    fprintf(fimage, "1\n");
    for (i = 0; i < 100; i++)
    {
        for (j = 0; j < 100; j++)
        {
            int temp = img[j][i];
            fprintf(fimage, "%d ", temp);
        }
    }
    fclose(fimage);
}

void init_identity(float x[3][3])
{
    int i, j;
    for (i = 0; i < 3; i++)
    {
        for (j = 0; j < 3; j++)
        {
            x[i][j] = (i == j) ? 1 : 0;
        }
    }
}

void draw_prllgram(struct parallelogram r, int shear, int img[100][100])
{
    BRHAM(r.center.x - r.a / 2 + shear, r.center.y - r.b / 2, r.center.x + r.a / 2 + shear, r.center.y - r.b / 2, img);
    BRHAM(r.center.x + r.a / 2 + shear, r.center.y - r.b / 2, r.center.x + r.a / 2, r.center.y + r.b / 2, img);
    BRHAM(r.center.x + r.a / 2, r.center.y + r.b / 2, r.center.x - r.a / 2, r.center.y + r.b / 2, img);
    BRHAM(r.center.x - r.a / 2, r.center.y + r.b / 2, r.center.x - r.a / 2 + shear, r.center.y - r.b / 2, img);
    //shear(-0.5);
}

void BRHAM(int X0, int Y0, int X1, int Y1, int img[100][100])
{
    int dx = X1 - X0;
    int dy = Y1 - Y0;
    int steps, i, pk;
    int X_change, Y_change;
    float m;
    int X = X0, Y = Y0;

    m = (float)abs(dy) / (float)abs(dx);

    if (dx != 0 && dy != 0)
    {
        if (m > (float)1)
        {
            Y_change = dy > 0 ? 1 : -1;
        }
        else if (m == (float)1)
        {
            Y_change = dy > 0 ? 1 : -1;
            X_change = dx > 0 ? 1 : -1;
        }
        else
        { //if ( m < (float)1){
            X_change = dx > 0 ? 1 : -1;
        }
    }
    else if (dy == 0)
    {
        Y_change = 0;
        X_change = dx > 0 ? 1 : -1;
    }
    else if (dx == 0)
    {
        Y_change = dy > 0 ? 1 : -1;
        X_change = 0;
    }

    steps = abs(dx) > abs(dy) ? abs(dx) : abs(dy);

    if (dx != 0 && dy != 0)
    {
        if (m >= (float)1)
        {
            pk = 2 * abs(dx) - abs(dy);

            for (i = 0; i <= steps; i++)
            {
                img[X][Y] = 0;

                if (pk < 0)
                {
                    pk = pk + 2 * abs(dx);
                    X_change = 0;
                }
                else
                {
                    pk = pk + 2 * abs(dx) - 2 * abs(dy);
                    X_change = dx > 0 ? 1 : -1;
                }

                X = X + X_change;
                Y = Y + Y_change;
            }
        }
        else if (m < 1)
        {
            pk = 2 * abs(dy) - abs(dx);

            for (i = 0; i <= steps; i++)
            {
                img[X][Y] = 0;

                if (pk < 0)
                {
                    pk = pk + 2 * abs(dy);
                    Y_change = 0;
                }
                else
                {
                    pk = pk + 2 * abs(dy) - 2 * abs(dx);
                    Y_change = dy > 0 ? 1 : -1;
                }

                X = X + X_change;
                Y = Y + Y_change;
            }
        }
    }
    else
    {
        for (i = 0; i <= steps; i++)
        {
            img[X][Y] = 0;
            X = X + X_change;
            Y = Y + Y_change;
        }
    }
}