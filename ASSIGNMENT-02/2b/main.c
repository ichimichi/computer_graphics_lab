#include <stdio.h>
#include <conio.h>
#include <math.h>
#include <stdlib.h>
#define H 100
#define W 100
#define PI 3.14159265

int img[H][W];

struct xy
{
    int x;
    int y;
};

struct ellipse
{
    struct xy center;
    int Rx;
    int Ry;
};

void rotateWithPivot(double theta, int xf, int yf);
void drawShape(struct ellipse e, double theta);
void plotPointsInQuadrants(int, int, int, int);
void midPointEllipse(struct ellipse e);
void MatMul(float mat1[3][3], int mat2[3][1], int res[3][1]);
void Translate(int xf, int yf);
void createPGM();
void init_identity(float x[3][3]);
void init_mat(int m[H][W], int x);
void overrideImage(int new[H][W]);
int abs(int x);

int main()
{
    struct ellipse e;
    int i, j;
    double theta;

    printf("\nEnter the center (x,y) : ");
    scanf("%d,%d", &e.center.x, &e.center.y);
    printf("\nEnter the semi-major axis (Rx)   : ");
    scanf("%d", &e.Rx);
    printf("\nEnter the semi-minor axis (Ry)   : ");
    scanf("%d", &e.Ry);
    printf("\nEnter theta   : ");
    scanf("%lf", &theta);

    drawShape(e, theta);

    createPGM();

    return 0;
}

void drawShape(struct ellipse e, double theta)
{
    init_mat(img, 1);
    midPointEllipse(e);
    rotateWithPivot(-theta, e.center.x - e.Rx, e.center.y);
}

void rotateWithPivot(double theta, int xf, int yf)
{
    float R[3][3];
    int P[3][1];
    int P1[3][1];
    int i, j;
    int img_temp[H][W];

    init_mat(img_temp, 1);

    theta = (theta * PI) / 180;

    R[0][0] = cos(theta);
    R[0][1] = -1 * sin(theta);
    R[0][2] = xf * (1 - cos(theta)) + yf * sin(theta);
    R[1][0] = sin(theta);
    R[1][1] = cos(theta);
    R[1][2] = yf * (1 - cos(theta)) - xf * sin(theta);
    R[2][0] = 0;
    R[2][1] = 0;
    R[2][2] = 1;

    for (i = 0; i < H; i++)
    {
        for (j = 0; j < W; j++)
        {
            if (img[i][j] == 0)
            {
                P[0][0] = i;
                P[1][0] = j;
                P[2][0] = 1;

                MatMul(R, P, P1);
                img_temp[P1[0][0]][P1[1][0]] = 0;
            }
        }
    }

    overrideImage(img_temp);
}

void Translate(int xf, int yf)
{
    float T[3][3];
    int P[3][1];
    int P1[3][1];
    int i, j;
    int img_temp[H][W];
    init_mat(img_temp, 1);

    init_identity(T);
    T[0][2] = xf;
    T[1][2] = yf;

    for (i = 0; i < H; i++)
    {
        for (j = 0; j < W; j++)
        {
            if (img[i][j] == 0)
            {
                P[0][0] = i;
                P[1][0] = j;
                P[2][0] = 1;
                MatMul(T, P, P1);
                img_temp[P1[0][0]][P1[1][0]] = 0;
            }
        }
    }

    overrideImage(img_temp);
}

void overrideImage(int new[H][W])
{
    int i, j;

    for (i = 0; i < H; i++)
    {
        for (j = 0; j < W; j++)
        {
            img[i][j] = new[i][j];
        }
    }
}

void MatMul(float mat1[3][3], int mat2[3][1], int res[3][1])
{
    int i, j, k;
    for (i = 0; i < 3; i++)
    {
        for (j = 0; j < 1; j++)
        {
            res[i][j] = 0;
            for (k = 0; k < 3; k++)
            {
                res[i][j] += (int)((mat1[i][k] * mat2[k][j]) + 0.5);
            }
        }
    }
}

void midPointEllipse(struct ellipse e)
{
    int pk;
    int Rx_sq = e.Rx * e.Rx;
    int Ry_sq = e.Ry * e.Ry;

    int two_Rx_sq = 2 * Rx_sq;
    int two_Ry_sq = 2 * Ry_sq;

    int x = 0;
    int y = e.Ry;

    int px = 0;
    int py = two_Rx_sq * e.Ry;

    plotPointsInQuadrants(e.center.x, e.center.y, x, y);

    pk = ((int)((Ry_sq + 0.25 * Rx_sq - Rx_sq * e.Ry) + 0.5));

    while (px < py)
    {

        x += 1;
        px += two_Ry_sq;

        if (pk < 0)
        {
            pk += px + Ry_sq;
        }
        else
        {
            y -= 1;
            py -= two_Rx_sq;
            pk += px + Ry_sq - py;
        }

        plotPointsInQuadrants(e.center.x, e.center.y, x, y);
    }

    pk = ((int)((Ry_sq * (x + 0.5) * (x + 0.5) + Rx_sq * (y - 1) * (y - 1) - Rx_sq * Ry_sq) + 0.5));

    while (y > 0)
    {
        y--;
        py -= two_Rx_sq;
        if (pk > 0)
        {
            pk += Rx_sq - py;
        }
        else
        {
            x++;
            px += two_Ry_sq;
            pk += Rx_sq - py + px;
        }

        plotPointsInQuadrants(e.center.x, e.center.y, x, y);
    }
}

void plotPointsInQuadrants(int xc, int yc, int x, int y)
{

    img[xc + x][yc + y] = 0;
    img[xc + x][yc - y] = 0;
    img[xc - x][yc + y] = 0;
    img[xc - x][yc - y] = 0;
}

int abs(int x)
{

    if (x >= 0)
    {
        return x;
    }
    else
    {
        return -1 * x;
    }
}

void init_mat(int m[H][W], int x)
{
    int i, j;
    for (i = 0; i < H; i++)
    {
        for (j = 0; j < W; j++)
        {
            m[i][j] = x;
        }
    }
}

void createPGM()
{
    FILE *pgmimg;
    int i, j;
    pgmimg = fopen("assignment-02b.pgm", "wb");
    fprintf(pgmimg, "P2\n");
    fprintf(pgmimg, "100 100\n");
    fprintf(pgmimg, "1\n");
    for (i = 0; i < H; i++)
    {
        for (j = 0; j < W; j++)
        {
            int temp = img[j][i];
            fprintf(pgmimg, "%d ", temp);
        }
    }
    fclose(pgmimg);
}

void init_identity(float x[3][3])
{
    int i, j;
    for (i = 0; i < H; i++)
    {
        for (j = 0; j < W; j++)
        {
            x[i][j] = (i == j) ? 1 : 0;
        }
    }
}