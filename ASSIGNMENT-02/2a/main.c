#include <stdio.h>
#include <conio.h>
#define H 100
#define W 100
#define COLOR 0
#define TOP 0
#define BOTTOM 1


int img[H][W];

struct xy{
    int x;
    int y;
};

struct circle{
    struct xy center;
    int radius;
};

struct line{
    struct xy from;
    struct xy to;
};

void drawShape( struct circle c );
void midPointCircleDraw( struct circle c );
void midPointSemiCircleDraw( struct circle c , int );
void plotPointsInOctants( int , int , int , int );
void plotPointsInFourOctants( int , int , int , int , int );

void lineBresenham( struct line );

int abs(int x){
    if(x>=0){
        return x;
    }
    else{
        return -1*x;
    }
}

void init_mat(int x){
    int i,j;
    for(i = 0;i < H;i++)
    {
        for(j = 0;j < W;j++)
        {
           img[i][j]=x;
        }
    }
}

void createPGM(){
    FILE* pgmimg;
    int i,j;
    pgmimg = fopen("assigment-02a.pgm","wb");
    fprintf(pgmimg,"P2\n");
    fprintf(pgmimg,"100 100\n");
    fprintf(pgmimg,"1\n");
    for(i = 0;i < H;i++)
    {
        for(j = 0;j < W;j++)
        {
           int temp = img[j][i];
           fprintf(pgmimg, "%d ", temp);
        }
    }
    fclose(pgmimg);
}

int main()
{
    struct circle c;
    int i,j;
    

    init_mat(1);

    printf("\nEnter the center (x,y) : ");
    scanf("%d,%d", &c.center.x , &c.center.y );
    printf("\nEnter the radius (r)   : ");
    scanf("%d", &c.radius );

    drawShape( c );

    createPGM();
    return 0;
}

void drawShape(struct circle c){
    struct circle cLeft;
    struct circle cRight;
    struct line l;


    cLeft.radius = cRight.radius = c.radius;
    cLeft.center.y = cRight.center.y = c.center.y;
    cLeft.center.x = c.center.x + 2 * c.radius ;
    cRight.center.x = c.center.x - 2 * c.radius ;

    l.from.x = cLeft.center.x + cLeft.radius ;
    l.from.y = cLeft.center.y ;
    l.to.x = cRight.center.x - cRight.radius ;
    l.to.y = cRight.center.y ;



    midPointSemiCircleDraw( cRight , BOTTOM );
    midPointCircleDraw( c );
    midPointSemiCircleDraw( cLeft , TOP );
    lineBresenham( l );

}

void midPointCircleDraw( struct circle c ){
    int x = 0;
    int y = c.radius;
    int pk = 1 - c.radius;



    plotPointsInOctants( c.center.x , c.center.y , x , y );
    while( x < y ){

	x += 1;

	if( pk > 0 )
	{
	    y -= 1;
	    pk = pk + 2*x - 2*y + 1;
	}else
	{
	    y -= 0;
	    pk = pk + 2*x + 1;
	}

	plotPointsInOctants( c.center.x , c.center.y , x , y );

    }

}


void midPointSemiCircleDraw( struct circle c , int position ){
    int x = 0;
    int y = c.radius;
    int pk = 1 - c.radius;


    plotPointsInFourOctants( c.center.x , c.center.y , x , y , position );
    while( x < y ){

        x += 1;

        if( pk > 0 )
        {
            y -= 1;
	    pk = pk + 2*x - 2*y + 1;
        }else
        {
            y -= 0;
            pk = pk + 2*x + 1;
        }

        plotPointsInFourOctants( c.center.x , c.center.y , x , y , position );

    }

}



void plotPointsInOctants( int xc, int yc, int x, int y ){

    img[xc + x][ yc + y] = COLOR;
    img[xc + x][ yc - y] = COLOR;
    img[xc - x][ yc + y] = 0;
    img[xc - x][ yc - y] = 0;
    img[xc + y][ yc + x] = 0;
    img[xc + y][ yc - x] = 0;
    img[xc - y][ yc + x] = 0;
    img[xc - y][ yc - x] = 0;


}

void plotPointsInFourOctants( int xc, int yc, int x, int y , int position ){
    if(position == 0)
    {
        img[xc + x][ yc + y] = 0;
        img[xc - x][ yc + y] = 0;
        img[xc + y][ yc + x] = 0;
        img[xc - y][ yc + x] = 0;
    }else
    {
        img[xc + x][ yc - y] = 0;
        img[xc - x][ yc - y] = 0;
        img[xc + y][ yc - x] = 0;
        img[xc - y][ yc - x] = 0;
    }


}



void lineBresenham( struct line l)
{
    int dx = l.to.x - l.from.x;
    int dy = l.to.y - l.from.y;
    int steps,i,pk;
    int X_change,Y_change;
    float m;
    int X = l.from.x , Y = l.from.y;

    m=(float)abs(dy)/(float)abs(dx);

    if(dx != 0 && dy!=0){
	if( m > (float)1){
		Y_change = dy > 0 ? 1 : -1;
	}
	else if ( m == (float)1){
		Y_change = dy > 0 ? 1 : -1;
		X_change = dx > 0 ? 1 : -1;
	}
	else if ( m < (float)1){
		X_change = dx > 0 ? 1 : -1;
	}

    }
    else if(dy == 0){
		Y_change = 0;
		X_change = dx > 0 ? 1 : -1;

    }
    else if(dx == 0){
		Y_change = dy > 0 ? 1 : -1;
		X_change = 0;

    }

    steps = abs(dx) > abs(dy) ? abs(dx) : abs(dy);

    if( dx != 0 && dy !=0)
    {
    if(m>= (float)1){
	pk = 2*abs(dx) - abs(dy);
	for ( i = 0; i <= steps; i++)
	{
		img[X][Y]=0;

		if(pk<0){
			pk=pk+2*abs(dx);
			X_change = 0;
		}
		else
		{
			pk=pk+2*abs(dx)-2*abs(dy);
			X_change = dx > 0 ? 1 : -1;
		}

		X = X + X_change;
		Y = Y + Y_change;

	}

    }
    else if(m<1){
	pk = 2*abs(dy) - abs(dx);

	for ( i = 0; i <= steps; i++)
	{
		img[X][Y]=0;

		if(pk<0){
			pk=pk+2*abs(dy);
			Y_change = 0;
		}
		else
		{
			pk=pk+2*abs(dy)-2*abs(dx);
			Y_change = dy > 0 ? 1 : -1;
		}

		X = X + X_change;
		Y = Y + Y_change;

	}

    }
    }
    else
    {
	for ( i = 0; i <= steps; i++)
	{
		img[X][Y]=0;
		X = X + X_change;
		Y = Y + Y_change;

	}
    }


}