#include <stdio.h>
#define LEFT 0
#define RIGHT 1
int img[100][100];
int color1 =0;
int color2= 255;
struct xy
{
    int x;
    int y;
};
struct circle
{
    struct xy center;
    int radius;
};

void midPointSemiCircleDraw(struct circle c, int position);
void plotPointsInFourOctants(int xc, int yc, int x, int y, int position);
void floodfill(int x,int y,int color);
void brham(int X0, int Y0, int X1, int Y1);
void drawShape(int width, int height,int x,int y);
void init_mat(int m[100][100], int x);
int abs(int x);
void createPGM();

main(int argc, char const *argv[])
{   
    int width;
    printf("Enter width: ");
    scanf("%d", &width);
    int height;
    printf("Enter height: ");
    scanf("%d", &height);
    int x,y;
    printf("Enter x: ");
    scanf("%d", &x);
    printf("Enter y: ");
    scanf("%d", &y);
    drawShape(width,height,x,y);
    floodfill(x,y,100);
    createPGM();
    return 0;
}

void floodfill(int x,int y,int color){
    if(img[x][y] == color1){
        img[x][y] = color;
        floodfill(x+1,y,color);
        floodfill(x-1,y,color);
        floodfill(x,y+1,color);
        floodfill(x,y-1,color);
    }

}

void drawShape(int w,int h,int x,int y)
{
    init_mat(img, color1);
    int nw = w/2;
    int nh = h/2;
    // brham(30, 40, 70, 40);
    // brham(30, 60, 70, 60);
    brham(x-nw, y+nh, x+nw, y+nh);
    brham(x-nw, y-nh, x+nw, y-nh);
    int radius = nh/2;
    struct circle c1, c2, c3, c4;
    c1.center.x = x-nw;
    c1.center.y = y+nh/2;
    c1.radius = c2.radius = c3.radius = c4.radius = radius;
    c2.center.x = x+nw;
    c2.center.y = y+nh/2;
    c3.center.x = x-nw;
    c3.center.y = y-nh/2;
    c4.center.x = x+nw;
    c4.center.y = y-nh/2;
    midPointSemiCircleDraw(c1, RIGHT);
    midPointSemiCircleDraw(c2, LEFT);
    midPointSemiCircleDraw(c3, LEFT);
    midPointSemiCircleDraw(c4, RIGHT);
    
}

void midPointSemiCircleDraw(struct circle c, int position)
{
    int x = 0;
    int y = c.radius;
    int pk = 1 - c.radius;

    plotPointsInFourOctants(c.center.x, c.center.y, x, y, position);
    while (x < y)
    {

        x += 1;

        if (pk > 0)
        {
            y -= 1;
            pk = pk + 2 * x - 2 * y + 1;
        }
        else
        {
            y -= 0;
            pk = pk + 2 * x + 1;
        }

        plotPointsInFourOctants(c.center.x, c.center.y, x, y, position);
    }
}

void plotPointsInFourOctants(int xc, int yc, int x, int y, int position)
{
    if (position == 0)
    {
        img[xc - x][yc + y] = color2;
        img[xc - x][yc - y] = color2;
        img[xc - y][yc + x] = color2;
        img[xc - y][yc - x] = color2;
    }
    else
    {
        img[xc + x][yc + y] = color2;
        img[xc + x][yc - y] = color2;
        img[xc + y][yc + x] = color2;
        img[xc + y][yc - x] = color2;
    }
}

void init_mat(int m[100][100], int x)
{
    for (int i = 0; i < 100; i++)
    {
        for (int j = 0; j < 100; j++)
            m[i][j] = x;
    }
}

void brham(int X0, int Y0, int X1, int Y1)
{
    int dx = X1 - X0;
    int dy = Y1 - Y0;
    int steps, i, pk;
    int X_change, Y_change;
    float m;
    int X = X0, Y = Y0;

    m = (float)abs(dy) / (float)abs(dx);

    if (dx != 0 && dy != 0)
    {
        if (m > (float)1)
        {
            Y_change = dy > 0 ? 1 : -1;
        }
        else if (m == (float)1)
        {
            Y_change = dy > 0 ? 1 : -1;
            X_change = dx > 0 ? 1 : -1;
        }
        else
        { //if ( m < (float)1){
            X_change = dx > 0 ? 1 : -1;
        }
    }
    else if (dy == 0)
    {
        Y_change = 0;
        X_change = dx > 0 ? 1 : -1;
    }
    else if (dx == 0)
    {
        Y_change = dy > 0 ? 1 : -1;
        X_change = 0;
    }

    steps = abs(dx) > abs(dy) ? abs(dx) : abs(dy);

    if (dx != 0 && dy != 0)
    {
        if (m >= (float)1)
        {
            pk = 2 * abs(dx) - abs(dy);

            for (i = 0; i <= steps; i++)
            {
                img[X][Y] = color2;

                if (pk < 0)
                {
                    pk = pk + 2 * abs(dx);
                    X_change = 0;
                }
                else
                {
                    pk = pk + 2 * abs(dx) - 2 * abs(dy);
                    X_change = dx > 0 ? 1 : -1;
                }

                X = X + X_change;
                Y = Y + Y_change;
            }
        }
        else if (m < 1)
        {
            pk = 2 * abs(dy) - abs(dx);

            for (i = 0; i <= steps; i++)
            {
                img[X][Y] = color2;

                if (pk < 0)
                {
                    pk = pk + 2 * abs(dy);
                    Y_change = 0;
                }
                else
                {
                    pk = pk + 2 * abs(dy) - 2 * abs(dx);
                    Y_change = dy > 0 ? 1 : -1;
                }

                X = X + X_change;
                Y = Y + Y_change;
            }
        }
    }
    else
    {
        for (i = 0; i <= steps; i++)
        {
            img[X][Y] = color2;
            X = X + X_change;
            Y = Y + Y_change;
        }
    }
}

int abs(int x)
{
    if (x >= 0)
        return x;
    else
        return -1 * x;
}

void createPGM()
{
    FILE *pgmimg;
    int i, j;
    pgmimg = fopen("assignment-05.pgm", "wb");
    fprintf(pgmimg, "P2\n");
    fprintf(pgmimg, "100 100\n");
    fprintf(pgmimg, "255\n");

    for (i = 0; i < 100; i++)
    {
        for (j = 0; j < 100; j++)
        {
            int temp = img[j][i];
            fprintf(pgmimg, "%d ", temp);
        }
    }
    fclose(pgmimg);
}